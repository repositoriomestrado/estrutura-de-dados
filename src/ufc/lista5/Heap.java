package ufc.lista5;

public class Heap {
	
	private int max; /* tamanho maximo do heap */
	private int pos; /* proxima posicao disponivel no vetor */
	private int[] prioridades; /* vetor das prioridades */
	
	public Heap(int max){
		this.max = max;
		this.prioridades = new int[max];
		this.pos = 0;
	}
	private void troca(int no, int pai){
		int f = prioridades[no];
		prioridades[no] = prioridades[pai];
		prioridades[pai] = f;
	}
	
	private void corrigirAcima(int position){
		
		while(position > 0){
			int pai = (position-1)/2;
			//int pai = (int)Math.ceil((double)(position-1)/2);
			if(prioridades[pai] < prioridades[position]){
				troca(position,pai);
			}else{
				break;
			}
			position=pai;
		}
		
	}
	
	public void inserirElemento(int valor){
		if(pos<max-1){
			prioridades[pos] = valor;
			corrigirAcima(pos);
			pos++;
		}else{
			System.out.println("Heap cheio!");
		}
	}
	
	
	public int buscarElemento(int pai, int elemento){
		
		while(pai>max-1){
			return -1;
		}
		if(elemento==prioridades[pai]){
			System.out.println("Elemento encontrado na posição: " + pai);
			return pai;
		}else{
			int filho_esq=2*pai+1;
			int filho_dir=2*pai+2;
			int e = buscarElemento(filho_esq, elemento);
			int d = buscarElemento(filho_dir, elemento);
			
			if(e>0){
				return e;
			}
			if(d>0){
				return d;
			}
			return -1;
		}
	}
	
	public void imprimirHeap(){
		if(pos==0){
			System.out.println("Heap vazio");
		}else{	
			for(int i=0;i<max;i++){
				if(prioridades[i]!=0){
					System.out.println("[" + i + "]=" + prioridades[i]);
				}
			}
		}
	}
	
	
	private void corrige_abaixo(int pai){
		while (2*pai+1 < pos){
			int filho_esq=2*pai+1;
			int filho_dir=2*pai+2;
			int filho;
			if (filho_dir >= pos){
				filho_dir=filho_esq;
			}
			if (prioridades[filho_esq] > prioridades[filho_dir]){
				filho=filho_esq;
			}else{
				filho=filho_dir;
			}
			if (prioridades[pai] < prioridades[filho]){
				troca(pai,filho);
			}else{
				break;
			}
			pai=filho;
		}
	}
	
	public void alterarElemento(int index, int novo_valor){
		prioridades[index] = novo_valor;
		corrigirAcima(index);
		corrige_abaixo(index);
	}
	
	public void liberaLista(){
		for(int i=0;i<max-1;i++){
			prioridades[i] = 0;
		}
		pos = 0;
	}
	
	public int removerElemento(int valor){
		if (pos>0) {
			int index = buscarElemento(0,valor);
			if(index<0){
				System.out.println("Elemento não encontrado");
				return -1;
			}
			prioridades[index]=prioridades[pos-1];
			prioridades[pos-1] = 0;
			pos--;
			corrige_abaixo(index);
			return 0;
		}else {
			System.out.println("Heap VAZIO!");
			return -1;
		}
	}
	
	public int getMax() {
		return max;
	}
	public void setMax(int max) {
		this.max = max;
	}
	public int getPos() {
		return pos;
	}
	public void setPos(int pos) {
		this.pos = pos;
	}
	public int[] getPrioridades() {
		return prioridades;
	}
	public void setPrioridades(int[] prioridades) {
		this.prioridades = prioridades;
	}
	

}
