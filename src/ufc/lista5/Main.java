package ufc.lista5;

public class Main {

	public static void main(String[] args) {
		// 1.  Criar a estrutura de dados (Construção da Heap);
		Heap heap = new Heap(10);
		
		// 2.  Inserir um elemento;
		heap.inserirElemento(11);
		heap.inserirElemento(5);
		heap.inserirElemento(8);
		heap.inserirElemento(3);
		heap.inserirElemento(4);
		heap.inserirElemento(2);
		heap.inserirElemento(1);
		heap.inserirElemento(9);
		heap.inserirElemento(10);
		heap.inserirElemento(7);

		//heap.inserirElemento(15);
		
		// 3.  Recuperar/Buscar um determinado elemento;
		//heap.buscarElemento(0,1);
		
		// 4.  Remover um determinado elemento;
		//heap.removerElemento(3);
		//heap.imprimirHeap();

		// 5.  Alterar o valor de um determinado elemento;
		//heap.alterarElemento(8,7);
		
		// 6.  Liberar a estrutura de dados;
		heap.liberaLista();
	}

}
