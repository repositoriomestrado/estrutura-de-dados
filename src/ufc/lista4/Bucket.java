package ufc.lista4;

public class Bucket {

	private int id;
	private int profundidadeLocal = 1;
	private int valor;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getProfundidadeLocal() {
		return profundidadeLocal;
	}
	public void setProfundidadeLocal(int profundidadeLocal) {
		this.profundidadeLocal = profundidadeLocal;
	}
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	} 
	
	
	
}
