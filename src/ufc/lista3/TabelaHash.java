package ufc.lista3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TabelaHash {
	
	private int tamanhoTabela;
	private List<List<Integer>> tabela = new ArrayList<List<Integer>>();    
    
    
	public TabelaHash(int n){
		this.tamanhoTabela = n/2;
	}
	
	public int calculaChaveDaTabela(int numero){
		  return numero%tamanhoTabela;
    }
	
	public void criarHash(){
		for (int i = 0; i < tamanhoTabela; i++) {
		      List<Integer> lista = new ArrayList<Integer>();
		      tabela.add(lista);
		  }	
		
	}
	public void inserirElemento(int value){
		int index = calculaChaveDaTabela(value);
		tabela.get(index).add(value);
	}
	public void imprimirLista(){
		for(int i=0;i<tamanhoTabela;i++){
			List<Integer> lista = tabela.get(i);
			if(lista.size()!=0){
				System.out.print("Posição["+i+"]:");
				for(Integer elemento:lista){
					System.out.print("("+elemento+")");
				}
				System.out.println("");
			}		
		}
	}
	public void buscarElemento(int valor){
		if(buscar(valor)>0){
			System.out.println("Elemento " + valor + " encontrado!");
		}else{
			System.out.println("Elemento "+ valor + " não encontrado");
		}
	}
	public int buscar(int valor){
		int index = calculaChaveDaTabela(valor);
		for(Integer i:tabela.get(index)){
			if(i.equals(valor)){
				return valor;
			}
		}
		return -1;
		
	}
	public void removeElemento(int valor){
		if(remover(valor)==0){
			System.out.println("Elemento " + valor + " removido!");
		}else{
			System.out.println("Elemento "+ valor + " não encontrado");
		}
	}

	
	public int remover(int valor){
		int index = calculaChaveDaTabela(valor);
		List<Integer> lista = tabela.get(index);
		for(int i=0;i<lista.size();i++){
			if(lista.get(i).equals(valor)){
				tabela.get(index).remove(i);
				return 0;
			}
		}
		return -1;
		
	}
	public void liberarLista(){
		for (int i = 0; i < tamanhoTabela; i++) {
		      List<Integer> lista = new ArrayList<Integer>();
		      tabela.add(null);
		 }	
		System.out.println("Lista liberada com sucesso!");
		
	}

}
