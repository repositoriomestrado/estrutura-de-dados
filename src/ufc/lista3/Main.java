package ufc.lista3;

public class Main {

	public static void main(String[] args) {
		
		TabelaHash hash = new TabelaHash(10);
		hash.criarHash();
		hash.inserirElemento(5);
		hash.inserirElemento(9);
		hash.inserirElemento(19);
		hash.imprimirLista();
		hash.buscarElemento(20);
		//hash.removeElemento(5);
		hash.imprimirLista();
		hash.liberarLista();
		hash.imprimirLista();
	}

}
